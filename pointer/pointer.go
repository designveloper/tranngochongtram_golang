package main

import (
	"fmt"
)

type myT struct {
	o int
}

func test(t myT) {
	t.o = 100
}

func testPointer(t *myT) {
	t.o = 100
}

func main() {
	x := myT{o: 0}
	y := &myT{o: 1}

	test(x)
	testPointer(y)
	// testPointer(&x)

	fmt.Printf("%v\n, %v\n", x, y)
}
