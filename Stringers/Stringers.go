package main

import "fmt"

type Abc struct {
	X int
	Y string
}

type W int

func (s Abc) String() string {
	return fmt.Sprintf("...%v...%b", s.Y, s.X)
}

func (v W) String() string {
	return fmt.Sprintf("[%b]", v)
}

func main() {
	i := Abc{0, "Arthur Dent"}
	o := W(6)
	fmt.Println(i, o)
}
