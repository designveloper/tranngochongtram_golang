package main

import "fmt"

func fibonacci() func(int) int {
	a := 0
	b := 1
	return func(x int) int {
		c := a + b
		if x == 0 {
			return a
		} else if x == 1 {
			return b
		} else {
			a = b
			b = c
			return c
		}
	}
}

func main() {
	f := fibonacci()
	for i := 0; i < 10; i++ {
		fmt.Println(f(i))
	}
}
