package main

import "fmt"

type vertex struct {
	X, Y float64
}

func (k *vertex) scale(f float64) {
	k.X = k.X * f
	k.Y = k.Y * f
}

func scaleFunc(z *vertex, f float64) {
	z.X = z.X * f
	z.Y = z.Y * f
}

func main() {
	v := vertex{3, 4}
	v.scale(2)
	scaleFunc(&v, 10)

	p := &vertex{4, 3}
	p.scale(3)
	scaleFunc(p, 8)

	fmt.Println(v, p)
}
