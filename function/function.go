package main

import (
	"fmt"
)

func main() {
	a := []byte("Hello")
	b := string(a)
	fmt.Println("This is byte form:", a)
	fmt.Println("This is a string:", b)
}
