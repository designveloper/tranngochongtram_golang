package main

import (
	"fmt"
	"runtime"
	"time"
)

func say(s string) {
	for i := 0; i < 5; i++ {
		time.Sleep(1 * time.Millisecond)
		fmt.Println(s)
	}
}

func main() {
	runtime.GOMAXPROCS(2)
	go say("world")
	say("hello")

	time.Sleep(100 * time.Millisecond)
}
