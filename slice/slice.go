package main

import "fmt"

type IPAddr [4]byte

func (v IPAddr) String() string {
	var a string
	for _, value := range v {
		a += fmt.Sprintf("%v.", value)
	}
	a = a[:len(a)-1]
	return a
}

func main() {
	hosts := map[string]IPAddr{
		"loopback":  {127, 0, 0, 1},
		"googleDNS": {8, 8, 8, 8},
	}
	for name, ip := range hosts {
		fmt.Printf("%v: %v\n", name, ip)
	}
}
