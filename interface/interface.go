package main

import (
	"fmt"
	"strconv"
)

type Writer interface {
	Write([]byte) (int, error)
	Test(int) (int, error)
}

type ConsoleWriter struct{}

type ConsoleWriter2 struct{}

func (w ConsoleWriter) Write(data []byte) (int, error) {
	n, err := fmt.Println(string(data))
	return n, err
}

func (w ConsoleWriter) Test(num int) (int, error) {
	d := int64(num)
	n, err := fmt.Println(strconv.FormatInt(d, 2))
	return n, err
}

func (m ConsoleWriter2) Write(data []byte) (int, error) {
	n, err := fmt.Println(string(data), "123")
	return n, err
}

func (w ConsoleWriter2) Test(num int) (int, error) {
	n, err := fmt.Println(num)
	return n, err
}

func main() {
	var w Writer = ConsoleWriter{}
	var m Writer = ConsoleWriter2{}
	w.Write([]byte("Hello"))
	w.Test(39)
	m.Write([]byte("Go"))
	m.Test(7)
}
