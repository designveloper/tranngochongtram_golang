package main

import (
	"fmt"
)

var (
	toBe   = true
	maxInt = 1 << 6
	z      = 6i
	s      = "hello"
)

func main() {
	fmt.Printf("Type: %T Value: %#v\n", toBe, toBe)
	fmt.Printf("Type: %T Value: %v\n", maxInt, maxInt)
	fmt.Printf("Type: %T Value: %v\n", z, z)
	fmt.Printf("Type: %T Value: %#v\n", s, s)
}
